# Ansible LAMP Collection Demo

This is a simple example playbook to demonstrate how to use Ansible Collections. Therefore the playbook is not supposed to be used in any production scenario and only provided for learning purposes.

Read the full [Documentation](https://www.jung-christian.de).
